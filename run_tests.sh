#!/bin/bash
DIR=$(pwd)
for TEST_FOLDER in ./tests/* 
do
    echo "Running $TEST_FOLDER"
    cd $TEST_FOLDER
    bash run.sh
    if [ "$?" = "0" ] ; then
        echo "Test $TEST_FOLDER ran successfully"
    else
        echo "Test $TEST_FOLDER finished with errors"
    fi
done
    