#!/bin/bash
###### Local Variables ######
TMP_DIR=".tagger"
BACKUP_DIR="backups"
PATCH_DIR="patched"
# Supported types we want. Some distros have their own stuff
TYPES=(statefulset deployment daemonset cronjob)
# For pod testing
TEST_POD_NAME="migration-test-pod"
POD_TIMEOUT=30s # How long we should wait for the pod to be downloaded
PROPER_SED="/usr/local/bin/gsed" # Workaround for Mac OS, install it with 'brew install gnu-sed'
###### /Local Variables #####

# Prechecks
if [ -z $LOCAL_REGISTRY ] ; then
    echo "Set the LOCAL_REGISTRY variable to your registry!"
    exit 1
fi
# /Prechecks

# Make temporary directory for scratch
echo "Creating temporary directory $TMP_DIR"
mkdir -p $TMP_DIR

# Mac OS Switcheroo
# We use proper-sed to wrap the functionality of sed. In linux proper-sed is linked to the actual sed. In Mac OS we link it to $PROPER_SED which defaults to gsed
if [ "$(uname)" = "Linux" ] ;
then
echo "Running in GNU/Linux"
    ln -s /usr/bin/sed ./$TMP_DIR/proper-sed >> /dev/null 2>&1 || true
else
    echo "Not running in GNU/Linux, implementing shenanigans"
    ln -s $PROPER_SED ./$TMP_DIR/proper-sed >> /dev/null 2>&1 || true
fi
###################

while getopts 'van:yd' OPTION; do
  case "$OPTION" in
    v)
      echo "Debug mode = ON"
      DEBUG=1
      ;;
    a)
      NAMESPACES=$(kubectl get namespaces -A -o=custom-columns=NAME:.metadata.name | ./$TMP_DIR/proper-sed -e "1d")
      ALL_NAMESPACES=1
      ;;
    n)
      NAMESPACES="$OPTARG"
      # Get the namespace and if it doesnt work exit with 1
      # If namespace exists, wc -l will output 2, otherwise 0
      OUT=$(kubectl get namespace $NAMESPACES | wc -l| ./$TMP_DIR/proper-sed 's/^ *//g')
      if [ "$OUT" = "0" ]; then
        echo "We couldnt get the namespace '$NAMESPACES'. Try running 'kubectl get namespace $NAMESPACES'"
        exit 1
      fi
      ;;
    y)
      echo "Auto approve is ON, I wont bother you with silly confirmations, those are for losers and production breaking people"
      AUTO_APPROVE=1
      ;;
    d)
      echo "Will delete backup and patched folders after the run"
      DELETE_BACKUPS_AND_PATCHES=1
      ;;
    ?)
      echo "script usage: $0 [-v] [-a] [-n namespace]" >&2
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"
function log() {
    printf "$(date -Iseconds) - INFO - $1 \n"
}
function debug() {
    if [ "$DEBUG" = "1" ]; then
        printf "$(date -Iseconds) - DEBUG - $1 \n"
    fi
}
function error() {
    printf "$(date -Iseconds) - ERROR - $1 \n"
}
function gettype(){
    for TYPE in ${TYPES[@]}
    do
        kubectl get -n $NAMESPACE $TYPE $1  > /dev/null 2>&1
        if [ $? = 0 ]; then
            echo $TYPE
            break
        fi
        #echo "$TYPE Exited with $?"
    done
}
function backup(){
    # Create directory for backing up resources
    mkdir -p $BACKUP_DIR >/dev/null 2>&1
    # Create working directory for patches
    mkdir -p $PATCH_DIR/ >/dev/null 2>&1
    debug "Backing up $1 '$2'"
    kubectl get -n $NAMESPACE $1 $2 -ojson | jq 'del(.metadata.namespace,.metadata.resourceVersion,.metadata.uid) | .metadata.creationTimestamp=null' > $TMP_DIR/tmp_backup.json
    if [ -f "$BACKUP_DIR/$3-$1-$2.json" ]; then
        log "The object is already backed up, saving another copy."
        cp $TMP_DIR/tmp_backup.json $BACKUP_DIR/$3-$1-$2-$(date -Iseconds).json
        BACKUP=$BACKUP_DIR/$3-$1-$2-$(date -Iseconds).json
    else
        cp $TMP_DIR/tmp_backup.json $BACKUP_DIR/$3-$1-$2.json
        BACKUP=$BACKUP_DIR/$3-$1-$2.json
    fi
    cp $BACKUP_DIR/$3-$1-$2.json $PATCH_DIR/$3-$1-$2.json
    debug "Backed up to $BACKUP"
}
function getimages(){
    # The image line can be one of the following:
    # '- image:' If the image was defined as the first element in the array
    # 'image:' If the image is not the first element of te array
    # First we remove the spaces from the beggining with regex
    # Then we remove '- ' from '- image' if it exists
    # Finally we remove 'image:'
    LINES=$(kubectl get -n $1 $2 $3 -oyaml | grep image: | ./$TMP_DIR/proper-sed 's/^ *//g' | ./$TMP_DIR/proper-sed 's#- ##g' | ./$TMP_DIR/proper-sed 's#image: ##g')
    echo $LINES > $TMP_DIR/images
}
function download(){
    # Downloads image $1
    debug "Downloading $1"
    OUT=$(docker pull $1 2>&1)
    if [[ "$?" = "0" ]]; then
        debug "Downloaded $1 successfully"; 
    else
        error "Error downloading $1. \n $OUT"
        stop_execution
    fi
}
function tag(){
    # Tags the images $1 to $2
    debug "Tagging $1 to $2 "
    OUT=$(docker tag $1 $2 2>&1)
    if [ "$?" = "0" ]; then
        debug "Tagged $2 successfully"; 
    else
        error "Error tagging $2. \n $OUT"
        stop_execution
    fi
}
function upload(){
    # Uploads image $1
    debug "Uploading $1"
    OUT=$(docker push $1 2>&1)
    if [ "$?" = "0" ]; then
        debug "Uploaded $1 successfully"; 
    else
        error "Error uploading $1 \n $OUT \n This might be a common error with some registries. Use 'docker push $1' for troubleshooting."
        stop_execution
    fi
}
function delete(){
    # Deletes image $1 from the local computer
    debug "Deleting $1 from the local computer"
    OUT=$(docker image rm $1 2>&1)
    if [ "$?" = "0" ]; then
        debug "Deleted $1 successfully"; 
    else
        error "Error deleting $1. \n $OUT"
        stop_execution
    fi
}
function stop_execution(){
    log "Exiting. The progress is still saved in the "$TMP_DIR" directory, delete it to start over or run again to continue. If you need to restore a backup make sure you add the namespace to kubectl."
    exit 1
}
function patch(){
    # Modifies the image from the object and saves the patched object to the patched folder
    NAMESPACE=$1
    TYPE=$2
    OBJECT=$3
    ORIGINAL_IMAGE=$4
    NEW_IMAGE=$5
    debug "Patching $2 '$3' on namespace $1"
    mkdir -p $PATCH_DIR
    ESCAPED_ORIGINAL_IMAGE=$(echo $ORIGINAL_IMAGE | ./$TMP_DIR/proper-sed "s#/#\\\\/#g")
    ESCAPED_NEW_IMAGE=$(echo $NEW_IMAGE | ./$TMP_DIR/proper-sed "s#/#\\\\/#g")
    ./$TMP_DIR/proper-sed -i s/$ESCAPED_ORIGINAL_IMAGE/$ESCAPED_NEW_IMAGE/g $PATCH_DIR/$NAMESPACE-$TYPE-$OBJECT.json
}
function apply(){
    # Applies the changes from the patched file
    NAMESPACE=$1
    TYPE=$2
    OBJECT=$3
    debug "Applying $2 '$3' on namespace $1"
    OUT=$(kubectl apply -f $PATCH_DIR/$NAMESPACE-$TYPE-$OBJECT.json --namespace=$NAMESPACE)
    if [ "$?" = "0" ]; then
        debug "Applied $NAMESPACE-$TYPE-$OBJECT.json successfully"; 
    else
        error "Error applying $NAMESPACE-$TYPE-$OBJECT.json. \n $OUT"
        stop_execution
    fi
}
function check_apply(){
    # Check with rollout if it is supported, or create a pod and wait for it to be up.
    NAMESPACE=$1
    TYPE=$2
    OBJECT=$3
    IMAGE=$4
    # If this is a deployment, daemonset or statefulset wait for the rollout
    SUPPORTED_ROLLOUT=(statefulset deployment daemonset)
    if printf '%s\0' "${SUPPORTED_ROLLOUT[@]}" | grep -Fxqz "$TYPE"; then
        debug "Waiting for rollout to finish"
        ROLLOUT=$(kubectl rollout status -n $NAMESPACE $TYPE $OBJECT)
        if [ "$?" = "0" ]; then
            debug "Rollout finished successfully"; 
        else
            error "Error rolling out. \n $OUT"
            stop_execution
        fi
    else
        # If this is a not a deployment, daemonset or statefulset create a pod and delete it if everything goes well
        debug "Deploying test pod '$TEST_POD_NAME'"
        POD=$(kubectl run $TEST_POD_NAME -n $NAMESPACE --image=$IMAGE && kubectl wait --for=condition=ready --timeout=$POD_TIMEOUT pod -n $NAMESPACE $TEST_POD_NAME)
        if [ "$?" = "0" ]; then
            debug "Pod deployed was created, went into ready state. Destroying it.";
            kubectl delete pod -n $NAMESPACE $TEST_POD_NAME >> /dev/null
        else
            # Not all images run out of the box. As long as the state is not ErrImagePull or ImagePullBackoff we are good.
            POD_STATUS=$(kubectl get pods -n $NAMESPACE $TEST_POD_NAME  | grep 'ErrImagePull\|ImagePullBackOff')
            # Grep returns 1 if it didnt find any occurrences
            if [ "$?" = "1" ]; then
                debug "Pod deployment failed but not due to image errors. Keep calm and carry on"
            else
                error "Error Testing pod. \n Run Output: \n $POD \n $(kubectl get pods -n $NAMESPACE $TEST_POD_NAME)\n"
                stop_execution
            fi
        fi
    fi    
}

error tuvieja

echo "We are going to process the following namespaces: $NAMESPACES"
echo ""
if [ "$AUTO_APPROVE" = "1" ] ; then
echo "Let's GO!"
else
echo "Press any key to continue..."
read
fi

for NAMESPACE in $NAMESPACES
do
    log "Processing namespace: $NAMESPACE"
    for TYPE in ${TYPES[@]}
    do
        log "Processing type: $TYPE"
        OBJECTS=$(kubectl get $TYPE -n $NAMESPACE -o=custom-columns=NAME:.metadata.name | ./$TMP_DIR/proper-sed -e "1d")
        for OBJECT in ${OBJECTS[@]}
        do
            log "Processing object: $OBJECT"
            if [ "$AUTO_APPROVE" != "1" ] ; then
                echo "Press any key to continue..."
                read
            fi
            debug "Getting images for $TYPE $OBJECT"
            #IMAGES=$(getimages $NAMESPACE $TYPE $OBJECT )
            getimages $NAMESPACE $TYPE $OBJECT
            IMAGES=$(cat $TMP_DIR/images)
            for IMAGE in ${IMAGES[@]}
            do
                REGISTRY=$(echo "$IMAGE" | cut -d '/' -f 1)
                REGISTRY_PATH=$(echo "$IMAGE" | cut -d '/' -f 2-10)
                FILENAME=$(echo "$IMAGE" | ./$TMP_DIR/proper-sed "s#/#.#g")
                if [ -f "$TMP_DIR/$FILENAME" ]; then
                    log "Image $IMAGE has already been procesed, skipping retagging"
                    backup $TYPE $OBJECT $NAMESPACE
                    patch $NAMESPACE $TYPE $OBJECT $IMAGE "$LOCAL_REGISTRY/$REGISTRY_PATH"
                    apply $NAMESPACE $TYPE $OBJECT
                    check_apply $NAMESPACE $TYPE $OBJECT "$LOCAL_REGISTRY/$REGISTRY_PATH"
                else
                    log "Processing image $IMAGE"
                    # If it has dots before the first / its a custom registry
                    if [ "$(echo "$IMAGE" | cut -d '/' -f 1 | grep -F .)" = "" ] ; then
                        debug "Image is from dockerhub"
                        REGISTRY=""
                        REGISTRY_PATH=$IMAGE
                    else
                        if [ "$REGISTRY" = "$LOCAL_REGISTRY" ] ;
                        then
                            debug "Object is already using our image, skipping modifications"
                        else
                            debug "Image has a custom registry, overwriting it. '$IMAGE' will be '$LOCAL_REGISTRY/$REGISTRY_PATH'"
                        fi
                    fi
                    if [ "$REGISTRY" != "$LOCAL_REGISTRY" ] ;
                    then
                        backup $TYPE $OBJECT $NAMESPACE
                        download $IMAGE
                        tag $IMAGE $LOCAL_REGISTRY/$REGISTRY_PATH
                        upload "$LOCAL_REGISTRY/$REGISTRY_PATH"
                        #delete "$LOCAL_REGISTRY/$REGISTRY_PATH"
                        patch $NAMESPACE $TYPE $OBJECT $IMAGE "$LOCAL_REGISTRY/$REGISTRY_PATH"
                        apply $NAMESPACE $TYPE $OBJECT
                        check_apply $NAMESPACE $TYPE $OBJECT "$LOCAL_REGISTRY/$REGISTRY_PATH"
                        touch $TMP_DIR/$FILENAME
                    fi
                fi
            done
        done
    done
    log "Finished processing namespace $NAMESPACE. The following are the images that are running now: \n\n$(kubectl get pods -n $NAMESPACE -o jsonpath='{.items[*].spec.containers[*].image}' | tr -s '[[:space:]]' '\n' | sort | uniq)\n"

done

# Cleanup
log "Removing temporary directory '$TMP_DIR'"
rm -r $TMP_DIR
if [ "$DELETE_BACKUPS_AND_PATCHES" = "1" ] ;
then
    log "Removing backup and patches"
    rm -r $BACKUP_DIR > /dev/null 2>&1 || true
    rm -r $PATCH_DIR > /dev/null 2>&1 || true
fi

if [ "$ALL_NAMESPACES" = "1" ] ;
then
    log "The following are the images that are running now: \n\n$(kubectl get pods -A -o jsonpath='{.items[*].spec.containers[*].image}' | tr -s '[[:space:]]' '\n' | sort | uniq)\n"
fi
