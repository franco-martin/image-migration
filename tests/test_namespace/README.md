# Description
This test deploys a namespace and a basic deployment. Then runs the migration script with that namespace and validates the deployment has the new image in it.

# Requirements (in addition to all the script requirements)
* Working registry

# Steps
* Deploy namespace and deployment from resources.yml
* Run script
* Validate exit with 0
* Validate image of deployment
* Delete namespace