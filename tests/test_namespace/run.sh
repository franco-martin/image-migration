if [ -z $LOCAL_REGISTRY ] ; then
    echo "Set the LOCAL_REGISTRY variable to your registry!"
    exit 1
fi
# Deploy test resources
kubectl apply -f resources.yml
# Test with namespace
../../main.sh -y -v -n image-migration-test

if [ "$?" != 0 ]; then
    rm -r ./.tagger || true
    echo "Finished with errors"
    exit 1
fi

IMAGE=$(kubectl get deployment nginx-deployment -oyaml -n image-migration-test | grep image: | sed 's/^ *//g' | sed 's#- ##g' | sed 's#image: ##g')
# Delete namespace
kubectl delete namespace image-migration-test
if [ "$IMAGE" = "$LOCAL_REGISTRY/nginx:1.14.2" ] ; then
    exit 0
else
    echo "$IMAGE" is not  "$LOCAL_REGISTRY/nginx:1.14.2"
    exit 1
fi