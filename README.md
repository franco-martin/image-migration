# Description
This utility is designed to migrate images used in a kubernetes cluster to a custom registry. The process consists of going through namespaces, getting the objects and processing each image used.
The images are downloaded using docker, retagged and uploaded. Finally the utility modifies the running objects and either waits for rollouts or deploys a pod in the namespace to verify everything works.

This tool is most useful to migrate applications from third parties, like service meshes or ingress controllers.
# Prerequisites
* docker (Make sure you can run docker without sudo. See https://docs.docker.com/engine/install/linux-postinstall/)
* kubectl
* jq
* gsed (Only for running in mac OS. Install with 'brew install gnu-sed')

# Configuration
See the first lines of the script, there is a Local Variables section that will help you customize its behaviour. Pay special attention to the TYPES variable. That lets you manage what types of objects we will work with. Some distros have custom objects so make sure you add them if you so desire. The default should work for most clusters.
## Parameters
* Verbosity (-v)
* All namespaces (-a)
* Namespace (-n)
* Auto approve (-y)
* Destroy backup and patch NOT RECOMMENDED (-d)
## Environment Variables
### Required
* LOCAL_REGISTRY (The destination registry for all images)

# Getting started
In order to get familiarized with the utility, we recommend starting with a single namespace in verbose mode and confirming each object.
Make sure kubectl is configured properly and that you can get the pods
```
        kubectl get pods -n foobar
```
Make sure docker is properly configured and that you can push an image to your registry


Once everything is in good shape, we recommend opening a second terminal side by side and have the following command running to check the status of your pods every two seconds
```
        watch -n 2 kubectl get pods -n foobar
```
Once you got that running, run the following command to test in the 'foobar' namespace
```
        ./main.sh -n foobar -v
```
While that runs , look at your second terminal an see the pods going up and down. The output of the script should look something like
```
Creating temporary directory .tagger
Not running in GNU/Linux, implementing shenanigans
Debug mode = ON
We are going to process the following namespaces: foobar

Press any key to continue...

2022-05-22T19:55:33-03:00 - INFO - Processing namespace: foobar 
2022-05-22T19:55:33-03:00 - INFO - Processing type: statefulset 
2022-05-22T19:55:33-03:00 - INFO - Processing type: deployment 
2022-05-22T19:55:33-03:00 - INFO - Processing object: nginx-deployment 
Press any key to continue...

2022-05-22T19:55:34-03:00 - DEBUG - Getting images for deployment nginx-deployment 
2022-05-22T19:55:34-03:00 - INFO - Processing image nginx:1.14.2 
2022-05-22T19:55:34-03:00 - DEBUG - Image has a custom registry, overwriting it. nginx:1.14.2 will be registry.home.local/nginx:1.14.2 
2022-05-22T19:55:34-03:00 - DEBUG - Backing up deployment 'nginx-deployment' 
2022-05-22T19:55:34-03:00 - DEBUG - Backed up to backups/foobar-deployment-nginx-deployment.json 
2022-05-22T19:55:34-03:00 - DEBUG - Downloading nginx:1.14.2 
2022-05-22T19:55:37-03:00 - DEBUG - Downloaded nginx:1.14.2 successfully 
2022-05-22T19:55:37-03:00 - DEBUG - Tagging nginx:1.14.2 to registry.home.local/nginx:1.14.2  
2022-05-22T19:55:37-03:00 - DEBUG - Tagged registry.home.local/nginx:1.14.2 successfully 
2022-05-22T19:55:37-03:00 - DEBUG - Uploading registry.home.local/nginx:1.14.2 
2022-05-22T19:55:49-03:00 - DEBUG - Uploaded registry.home.local/nginx:1.14.2 successfully 
2022-05-22T19:55:49-03:00 - DEBUG - Deleting registry.home.local/nginx:1.14.2 from the local computer 
2022-05-22T19:55:49-03:00 - DEBUG - Deleted registry.home.local/nginx:1.14.2 successfully 
2022-05-22T19:55:49-03:00 - DEBUG - Patching deployment 'nginx-deployment' on namespace foobar 
2022-05-22T19:55:49-03:00 - DEBUG - Applying deployment 'nginx-deployment' on namespace foobar 
2022-05-22T19:55:49-03:00 - DEBUG - Applied foobar-deployment-nginx-deployment.json successfully 
2022-05-22T19:55:49-03:00 - DEBUG - Waiting for rollout to finish 
2022-05-22T19:55:52-03:00 - DEBUG - Rollout finished successfully 
2022-05-22T19:55:52-03:00 - INFO - Processing type: daemonset 
2022-05-22T19:55:52-03:00 - INFO - Processing type: cronjob 
2022-05-22T19:55:52-03:00 - INFO - Finished processing namespace foobar. The following are the images that are running now: 

nginx:1.14.2   <----- The old image is still there because the old pod is terminating
registry.home.local/nginx:1.14.2
 
2022-05-22T19:55:52-03:00 - INFO - Removing temporary directory '.tagger'
```
# Errors and troubleshooting
Failures in the script will terminate it. It runs in bash with the -e parameter, which means that if something exits with something other than 0, the script will fail. This is to ease debugging.

If something goes wrong, you can try adding the '-v' parameter to see what went wrong.
## Saving progress
Progress is saved automatically, when an image is processed successfully it creates a file with the name of the image without the slashes in the temporary folder. Whenever we process an image, we check that the file doesn't exist to skip processing it, saving you time.
## Backups
Every object we process is backed up to the backups folder as it is in the cluster with the resourceVersion, namespace, uid and creationTimestamp attributes removed so we can apply it immediatly if something goes wrong. YOU will have to do this, we'd rather have a human evaluate a failure has happened and restore the backup.

When we try to createb a backup of an object, we will check if a backup already exists. If it does, we will not overwrite it. Instead we will create another one with a timestamp appended to the name.

## Common errors
### Upload failed
Sometimes the 'docker push' command can present errors. The most common error we found is "unknown blob". The script will give you the command you need to run in order to troubleshoot it further.

### Failed deployment
When we patch deployments, statefulsets and daemonsets we wait for the rollout to succeed. If that doesn't happen use kubectl to troubleshoot what happened. When we patch an object like a job or cronjob, we deploy a pod in that namespace and validate it doesn't have image errors.

# Examples

## Work with namespace 'test' and confirm before each object
./main.sh -n test

## Work with namespace 'test'
./main.sh -n test

## Work with all namespaces without confirming
./main.sh -a -y
